# EXTENSION IDEAS
#
# Improve error handeling
import sys, getopt, os
from pathlib import Path
import argparse
import re

def main(name, argv):
    #create the top-level parser    
    parser = argparse.ArgumentParser(prog='RustToJasmin')
    subparsers = parser.add_subparsers(title='subcommands', dest="command")

    # create the parser for the "generate" command
    parser_generate = subparsers.add_parser('generate', help='Generate Jasmin function for Rust function marked with "// Jasmin" in current directory')

    # create the parser for the "build" command
    parser_build = subparsers.add_parser('build', help='Build the Jasmin and Rust code')
    parser_build.add_argument('compiler', type=Path, help='Specify the path to the Jasmin compiler')
    parser_build.add_argument('--opts', help='Specify Jasmin compiler options')

    parser_from_rlib = subparsers.add_parser('from-rlib', help='Generate Jasmin stub directly from Rlib file')
    parser_from_rlib.add_argument('input', type=Path, help='Specify the Rlib file to be converted')

    args = parser.parse_args()
    #if args
    if args.command == "generate":
        gen_jasmin()
    elif args.command == "build":
        build(args)
    elif args.command == "from-rlib":
        file = str(args.input)[len("input="):]
        function_name, parameter_names, rust_types, return_type = parse_rlib(file)
        # Failed to parse Rlib (TODO: Move error handling to this function)
        if function_name == "":
            sys.exit(2)
        verify_success, parameter_names, types, function_returns = expand_and_verify(parameter_names, rust_types, return_type)
        if verify_success:
            write_jasmin_file(function_name, parameter_names, rust_types, return_type, function_returns, file)
        else:
            print("Rust file: " + file + " does not meet the criteria to be converted to a Jasmin function")


def gen_jasmin():
    generated_jasmin_stubs = []
    rust_files = scan()
    if len(rust_files) < 1:
        print("No rust (.rs) files found in: " + os.getcwd())
        sys.exit(2)
    # Parse files for Jasmin attribute
    functions = parse(rust_files)
    print("[*] Generating jasmin function stubs for:\n")
    for function in functions:
        print("\t* " + get_function_name(function[1]) + " in " + function[0])
    # Move Rust functions into Rlib files
    print("\n[*] Generating Rust rlib files")
    rlib_files = to_rlib(functions)
    # Generate Jasmin files from Rlib files
    print("[*] Generating Jasmin stubs")
    for file in rlib_files:
        function_name, parameter_names, rust_types, return_type = parse_rlib(file[1])
        # Skip if parsing Rlib file fails (TODO: Move error handling here)
        if function_name == "":
            continue
        verify_success, parameter_names, types, function_returns, slices = expand_and_verify(parameter_names, rust_types, return_type)
        if verify_success:
            if write_jasmin_file(function_name, parameter_names, rust_types, slices, return_type, function_returns, file[1]):
                import_rlib(file[0], file[2], file[3])
                generated_jasmin_stubs.append((file[0], file[2], file[3]))
            else:
                continue
        else:
            print("Function: " + file[2] + " in rust file: " + file[0] + " does not meet the criteria to be converted to a Jasmin function")
            # remove created .rs file from Jasmin directory
            filename = file[3] + ".rs"
            os.chdir('jasmin')
            os.remove(filename)
            os.chdir('../')

    print("[*] Done")
    print("Please write Jasmin functions for:\n")
    for generated_jasmin_stub in generated_jasmin_stubs:
        print(generated_jasmin_stub[1] + " (" + generated_jasmin_stub[0] +" ) in file jasmin/"+ generated_jasmin_stub[2])

def build(args):
    cwd = os.getcwd()
    called_form_jasmin_dir = False
    if cwd.split('/')[-1] != "jasmin":
        os.chdir('jasmin')
    rust_files = scan()
    functions = parse2(rust_files)

    for function in functions:
        print("[*] Generating rlib for function: " + get_function_name(function[1]) + " in " + function[0])
        function_name, rust_filename = gen_jasmin_names(function)
        jasmin_filename = rust_filename.replace(".rs", ".jazz")
        jasmin_object_file = build_jasmin(jasmin_filename, args.compiler, args.opts)
        rlib_file = build_rlib(rust_filename)
        swap_object_files(rlib_file, jasmin_object_file)
    cargo_build_file(rlib_file)

def build_jasmin(jasmin_filename, jasmin_compiler, jasmin_compiler_opts):
    jasmin_asm_file = jasmin_filename.replace('.jazz', '.s')
    jasmin_object_file = jasmin_filename.replace('.jazz', '.o')
    print("[*] Compiling Jasmin file: " + jasmin_filename)
    if jasmin_compiler_opts is not None:
        os.system(str(jasmin_compiler)[len("compiler="):] + " " + str(jasmin_compiler_opts) + " " + jasmin_filename + " -o " + jasmin_asm_file)
    else:
        os.system(str(jasmin_compiler)[len("compiler="):] + " " + jasmin_filename + " -o " + jasmin_asm_file)
    os.system("clang " + jasmin_asm_file + " -c -o " + jasmin_object_file)
    return jasmin_object_file

def build_rlib(rust_filename):
    print("[*] Compiling Rlib")
    os.system('rustc --crate-type=rlib ' + rust_filename)
    name = rust_filename.split("/")[-1]
    new_name = "lib" + name.replace(".rs", ".rlib")
    return (rust_filename.replace(name, new_name))

def swap_object_files(rlib_file, jasmin_object_file):
    print("[*] Swapping Rust object file for Jasmin object file")
    os.system('ar -x ' + rlib_file)
    directory = os.getcwd()
    os.remove(rlib_file)
    files_in_directory = os.listdir(directory)
    filtered_files = [file for file in files_in_directory if file.endswith(".rcgu.o")]
    
    for file in filtered_files:
        path_to_file = os.path.join(directory, file)
        os.remove(path_to_file)
    os.system('ar -crs ' +rlib_file + " " + jasmin_object_file + " lib.rmeta")
    os.remove("lib.rmeta")

def cargo_build_file(rlib_file):
    print("[*] Rlib file(s) generated\n")
    print("In order to build the file(s) either use rustc directly:")
    print("\trustc main.rs -L " + rlib_file.rsplit('/', 1)[0] + "\n")
    print("Or when using Cargo add the following line to the build.rs file:")
    print("\tcargo:rustc-link-search=" + rlib_file.rsplit('/', 1)[0])
    return

def gen_jasmin_names(function):
    function_name = get_function_name(function[1])
    crate_name = function[0][:len(function[0])-3] 
    filename = os.getcwd() + "/" + crate_name + ".rs"
    return function_name, filename

# Get all Rust files in the current directory
def scan():
    rust_files = []
    for filename in os.listdir(os.getcwd()):
        # Only consider Rust files
        if filename.endswith(".rs"):
            rust_files.append(filename)
    return rust_files

def parse(rust_files):
    functions = []
    for file in rust_files:
        f = open(file, "r")
        lines = f.readlines()
        for i in range(0, len(lines)):
            if lines[i] == "// Jasmin\n":
                function = lines[i+1]
                functions.append((file, function))

    return functions

def parse2(rust_files):
    functions = []
    for file in rust_files:
        f = open(file, "r")
        lines = f.readlines()
        for i in range(0, len(lines)):
            if lines[i] == "#[no_mangle]\n":
                function = lines[i+1]
                functions.append((file, function))

    return functions



def to_rlib(functions):
    # list of generated rlib files
    files = []
    for function in functions:
        function_name = get_function_name(function[1])
        crate_name = function[0][:len(function[0])-3] + "_" + function_name
        # Include filename of file function is present in to avoid naming conflicts 
        filename = os.getcwd() + "/jasmin/" + crate_name + ".rs"
        os.makedirs(os.path.dirname(filename), exist_ok=True)

        # Check if the user wants to overwrite the existing rlib file
        if os.path.isfile(filename):
            overwrite = input('File: ' + filename + ' already exists. Overwrite? Y = yes, N = no\n')
            if overwrite.lower() != 'y':
                continue

        with open(filename, "w") as f:
            f.write("#[no_mangle]\n")
            if "pub fn " + function_name in function[1]:
                f.write(function[1])
            else:
                f.write("pub " + function[1])
            if ("->") in function[1]:
                # function returns, so add dummy value
                if ("bool") in function[1].split("->")[1]:
                    f.write("\tlet x = true;\n")
                    f.write("\tx")
                else:
                    f.write("\tlet x = 5;\n")
                    f.write("\tx")
            f.write("\n}")
        files.append((function[0], filename, function_name, crate_name))
    return files

    


def parse_rlib(file):
    names = []
    types = []
    return_type = []
    func_name = ""
    with open(file, "r") as f:
        lines = f.readlines()
        for i in range(0, len(lines)):
            # Verify Rlib file has correct format
            if lines[i] == "#[no_mangle]\n":
                if lines[i+1][0:6] == "pub fn":
                    function = lines[i+1][7:]
                    function_name, remainder = function.split("(", 1)
                    return_args = remainder.find(",")
                    # Only one argument
                    if return_args == -1:
                        arg_name, arg_type = remainder.split(":", 1)
                        names.append(arg_name)
                        arg_type, return_args = arg_type[1:].split(")")
                        if ("->") in return_args:
                            _, ret_type = return_args.split('>')
                            ret_type, _ = ret_type.split('{')
                            return_type.append(ret_type.replace(" ", ""))
                        arg_type = arg_type.replace(" ", "")
                        types.append(arg_type)
                        return function_name, names, types, return_type
                    # More than one argument
                    else: 
                        print(remainder)
                        remainder, return_args = remainder.split(")")
                        if ("->") in return_args:
                            _, ret_type = return_args.split('>')
                            ret_type, _ = ret_type.split('{')
                            return_type.append(ret_type.replace(" ", ""))
                        args = remainder.split(",")
                        args = map(lambda s: s.replace(" ", ""), args)
                        for arg in args:
                            name, rtype = arg.split(":", 1)
                            names.append(name)
                            types.append(rtype)
                        return function_name, names, types, return_type
    print("ERROR: ")
    print("Unable to parse Rust function in file: " + file + 
        "\nFunction expected to be public and to have #[no_mangle] attribute")
    return func_name, names, types, return_type

def import_rlib(filename, function_name, crate_name):
    # TODO: Function reads entire file into memory
    # TODO: Import rlib after outer doc comments 
    line = "extern crate " + crate_name + ";\nuse " + crate_name + "::" + function_name + ";"
    with open(filename, "r+") as f:
        content = f.read()
        f.seek(0, 0)
        f.write(line.rstrip('\r\n') + '\n' + content)
    
    with open(filename, "r") as f:
        buf = f.readlines()
        for i in range(0, len(buf)):
            if "fn " + function_name in buf[i]:
                count = 1
                index = i
                while count != 0:
                    line = buf[index+1]
                    for c in line:
                        if c == "{":
                            count +=1 
                        elif c == "}":
                            count -=1
                    index += 1
                break
    
    for y in range(0, index-i+1):
        buf[i+y] = "//" + buf[i+y]
    
    with open(filename, "w") as f:
        for line in buf:
            f.write(line)

def expand_and_verify(names, types, return_type):
    # The supported Rust parameter types
    rust_types = ['u8','u16','u32','u64','usize','*mutu8','*constu8','*mutu16',
                '*constu16','*mutu32','*constu32','*mutu64','*constu64']

    # The supported return types
    return_types = ['u8', 'u16', 'u32', 'u64']
    # The supported slices
    slice_types = ['&[u8]', '&mut[u8]','&[u16]','&mut[u16]','&[u32]','&mut[u32]','&[u64]','&mut[u64]']
    
    #assert that there are the same argument names as argument types
    assert(len(names) == len(types))

    #assert that there is at most 1 return type
    assert(len(return_type) < 2)

    slices = {}
    # If there is a slice, replace slices with 2 Jasmin arguments
    for i in range(0, len(types)):
        if types[i] in slice_types:
            slices[i] = types[i]
            name = names[i]
            types[i] = 'usize'
            types.insert(i + 1, 'usize')
            names.insert(i + 1, name + '_len')
    function_returns = False

    # Ensure no more then the available register are used
    if len(names) > 6:
        print("Exceeded maximum register count. A maximum of 6 registers are supported. Slices take up two registers")
        print("Current registers: " + str(names))
        return False, names, types, function_returns, slices

    # Check if there is a return argument, if it only has one return type and if it is one of the allowed types 
    if len(return_type) == 1:
        if return_type[0] in return_types:
            function_returns = True
        else:
            # Either more than one argument is returned or it is an incorrect type
            print("Function can only return one argument and must be one of the following types: " + str(return_types))
            return False, names, types, function_returns, slices
    
    regex = re.compile('&mut\[u\d+;\d+\]|&\[u\d+;\d+\]')
    for i in range(0, len(types)):
        if types[i] not in rust_types:
            if regex.match(types[i]):
                continue
            return False, names, types, function_returns, slices

    return True, names, types, function_returns, slices

def write_jasmin_file(function_name, parameter_names, rust_types, slices, return_type, function_returns, file):
    type_dict = {
            'u8': 'reg u8',
            'u16': 'reg u16',
            'u32': 'reg u32',
            'u64': 'reg u64',
            'usize': 'reg u64',
            '*mutu8': 'reg u64',
            '*constu8': 'reg u64',
            '*mutu16': 'reg u64',
            '*constu16': 'reg u64',
            '*mutu32': 'reg u64',
            '*constu32': 'reg u64',
            '*mutu64': 'reg u64',
            '*constu64': 'reg u64'
    }
    
    # Remove rust extension from rlib file to create Jasmin file
    output_file = file.replace(".rs", ".jazz")
    jasmin_entry_function = ""
    for i in range(0, len(rust_types)):
        if i in slices:
            jasmin_entry_function += "// slice: " + slices[i] +"\n"
            jasmin_entry_function += "//\t slice name: " + parameter_names[i] + "\n"
            jasmin_entry_function += "//\t slice length: " + parameter_names[i+1] + "\n"
            jasmin_entry_function += "//\t jasminc -checksafety -safetyparam \"" + parameter_names[i] + ";" + parameter_names[i+1] + "\"\n"
        else:
            if i > 0 and i-1 in slices:
                continue
            jasmin_entry_function += "// " + parameter_names[i] + ": " + rust_types[i] + "\n"
    if function_returns:
        jasmin_entry_function += "// Type of return argument: " + return_type[0] + "\n"

    jasmin_entry_function += "export fn " + function_name + "("
    jasmin_entry_function += type_dict.get(rust_types[0], "reg u64") + " " + parameter_names[0]
    prev_type = type_dict.get(rust_types[0], "reg u64")
    for i in range(1,len(parameter_names)):
        if prev_type == type_dict.get(rust_types[i], "reg u64"):
            jasmin_entry_function += " "
            jasmin_entry_function += parameter_names[i]
        else:
            jasmin_entry_function += ", "
            jasmin_entry_function += type_dict.get(rust_types[i], "reg u64")
            jasmin_entry_function += " "
            jasmin_entry_function += parameter_names[i]
        prev_type = type_dict.get(rust_types[i], "reg u64")

    jasmin_entry_function += ")"
    # Insert return argument when needed
    if function_returns:
        jasmin_entry_function += " -> " + type_dict[return_type[0]]

    jasmin_entry_function += "\n{ \n\n}"
    
    # Check if the user wants to overwrite the existing jasmin file
    if os.path.isfile(output_file):
        overwrite = input('File: ' + filename + ' already exists. Overwrite? Y = yes, N = no\n')
        if overwrite.lower() != 'y':
            return False
    with open(output_file, "w") as f:
        f.write(jasmin_entry_function)
    return True

def get_function_name(function):
    return function.split('(')[0].split(' ')[-1]


if __name__ == "__main__":
    main(sys.argv[0], sys.argv[1:])