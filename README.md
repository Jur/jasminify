# Jasminfiy 
Jasminify is a python tool that simiplifies the process of calling Jasmin (https://github.com/jasmin-lang/jasmin) code from Rust. 

Jasminify is written to replace existing Rust functions with Jasmin functions without relying on the `unsafe` keyword. This is accomplished by compiling a Rlib file with a skeleton Rust function (i.e. function with empty body or just a return statement) of the function the programmer wants to replace. After the compilation the Rust object file in the Rlib is replaced with the Jasmin object file. In this way Jasmin can be called from Rust.

Jasminify has two stages:

1. Generate, which scans the current directory for Rust files and generates Rlib's and Jasmin functions for each Rust function marked with "// Jasmin"

2. Build, which compiles the Rlib and Jasmin code and replaces the Rust object file with the Jasmin object file.

Currently the following Rust types are supported as parameters to be converted from Rust to Jasmin: 
* Unsigned integers
    * ```u8 u16 u32 u64 usize```
* Raw pointers:
    * ```*mut u8 *const u8 *mut u16 *const u16 *mut u32 *const u32 *mut u64 *const u64```
* References to Arrays (where `N` is the nubmer of elements in the array):
    * ```&[u8; N] &mut[u8; N] &[u16; N] &mut[u16; N] &[u32; N] &mut[u32; N] &[u64; N] &mut[u64; N]```
* Slices:
    * ```&[u8]  &mut[u8] &[u16] &mut[u16] &[u32] &mut[u32] &[u64] &mut[u64]```
        * As slices use two registers the slices Jasminify automatically creates an extra argument in the Jasmin function which represents the length. This variable keeps the original name with "_len" appended to it.

The following Rust return types are allowed: ```u8, u16, u32, u64```

## Safety
Jasminify ensures the following conditions are satisfied: 
* Only allowed types are used for parameters and return arguments
* Jasmin uses the type corresponding to the Rust type 
* The correct number of arguments are used
* The number of arguments is not greater than 6
* Only one return argument is used


Altough Jasminify prevents part of the errors there are still a safety considerations the programmer should keep in mind when calling Jasmin from Rust. 
- Jamsin is able to change the contents of immutable values on the heap through the pointers passed as arguments to the entry function.
- The safety preconditions of the Jasmin safety checker are satisfied

Furthermore currently we do not have a formal proof for the safety of the approach. Although we believe the s

## Example

The following Rust code is stored in the file example.rs:

```Rust
fn main() {
	let mut num = 3;
	println!("Call jazz add_three");
	num = add_three(num);
    println!("num: {}", num);

    let mut array = [1,2,3,4,5];
	println!("Call jazz increase_by_pos");
    increase_by_pos(&mut array[..]);
    println!("array: {:?}", array);
}

// Jasmin
fn increase_by_pos(x: &mut [u32]){
       for i in 0..x.len() {
           x[i] += i as u32;
       }
}

// Jasmin
fn add_three(num: u32) -> u32 {
    num + 3
}
```
The functions marked with "// Jasmin" (increase_by_pos & add_three) should be replaced with Jasmin code. 

First the Python tool is run to generate the Jasmin functions:
`python ../jasminify.py generate`

The tool output is as follows:

```
[*] Generating jasmin function stubs for:

	* increase_by_pos in example.rs
	* add_three in example.rs

[*] Generating Rust rlib files
[*] Generating Jasmin stubs
[*] Done
Please write Jasmin functions for:

increase_by_pos (example.rs ) in file jasmin/example_increase_by_pos
add_three (example.rs ) in file jasmin/example_add_three
```
The tool automatically comments out the Rust functions and imports the rlib files into example.rs. The Jasmin functions are genereated in a newly created Jasmin folder.
```
ls jasmin/
example_add_three.jazz  example_add_three.rs  example_increase_by_pos.jazz  example_increase_by_pos.rs
```
When we examine example_add_three.jazz and example_increase_by_pos.jazz the following Jasmin code is observed:

```
// num: u32
// Type of return argument: u32
export fn add_three(reg u32 num) -> reg u32
{

}
```

```
// slice: &mut[u64]
//   slice name: x
//   slice length: x_len
//   jasminc -checksafety -safetyparam "x;x_len"
export fn increase_by_pos(reg u64 x x_len)
{

}
```
The comments above the Jasmin functions indicate the Rust types of the Jasmin arguments. Next the Jasmin code is written:
```
// num: u32
export fn add_three(reg u32 num) -> reg u32
{ 
    reg u32 z;
    z = num;
    z += 3;
    return z;
}
```

```
// x: usize
// x_len: usize
export fn increase_by_pos(reg u64 x, reg u64 x_len)
{ 
    reg u64 i;
    i = 0;
    while(i < x_len) {
        [x+8*i] += i;
        i += 1;
    }
}
```
After writing these Jasmin functions the tool can be run again (from the directory containing example.rs) to compile the Jasmin code and move the Jasmin object code into the Rlib files. The path to the Jasmin compiler needs to be specified in order to compile the Jasmin files.

`python ../jasminify.py build compiler=../../../../jasmin2/jasmin-compiler-master/compiler/jasminc.native`

```
[*] Generating rlib for function: increase_by_pos in example_increase_by_pos.rs
[*] Compiling Jasmin file: <path>/example/jasmin/example_increase_by_pos.jazz
[*] Compiling Rlib
warning: unused variable: `x`
 --> <path>/example/jasmin/example_increase_by_pos.rs:2:24
  |
2 | pub fn increase_by_pos(x: &mut [u64]){
  |                        ^ help: if this is intentional, prefix it with an underscore: `_x`
  |
  = note: `#[warn(unused_variables)]` on by default

warning: 1 warning emitted

[*] Swapping Rust object file for Jasmin object file
[*] Generating rlib for function: add_three in example_add_three.rs
[*] Compiling Jasmin file: <path>/example/jasmin/example_add_three.jazz
[*] Compiling Rlib
warning: unused variable: `num`
 --> <path>/example/jasmin/example_add_three.rs:2:18
  |
2 | pub fn add_three(num: u32) -> u32 {
  |                  ^^^ help: if this is intentional, prefix it with an underscore: `_num`
  |
  = note: `#[warn(unused_variables)]` on by default

warning: 1 warning emitted

[*] Swapping Rust object file for Jasmin object file
[*] Rlib file(s) generated

In order to build the file(s) either use rustc directly:
	rustc main.rs -L <path>/example/jasmin

Or when using Cargo add the following line to the build.rs file:
	cargo:rustc-link-search=<path>/example/jasmin
```
The Rust compiler warns about some unused variables, these warning are present as the Rust skeleton functions do not actually use the values we provide to them as argument. However the Jasmin code does use the argumets, therefore these warnings can be ignored. As the tool specifies the project can now be build by running:
`rustc example.rs -L jasmin/`
