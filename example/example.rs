fn main() {
	let mut num = 3;
	println!("Call jazz add_three");
	num = add_three(num);
    println!("num: {}", num);

    let mut array = [1,2,3,4,5];
	println!("Call jazz increase_by_pos");
    increase_by_pos(&mut array[..]);
    println!("array: {:?}", array);
}

fn increase_by_pos(x: &mut [u64]){
       for i in 0..x.len() {
           x[i] += i;
       }
}

// Jasmin
fn add_three(num: u32) -> u32 {
    num + 3
}
